import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainMenuComponent } from './views/main-menu/main-menu.component';
import { PokemonCardComponent } from './components/pokemon-card/pokemon-card.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { DetailedPokemonComponent } from './components/detailed-pokemon/detailed-pokemon.component';
import { TypeBlockComponent } from './components/type-block/type-block.component';
import { SpeechSynthesisModule } from '@kamiazya/ngx-speech-synthesis';

@NgModule({
  declarations: [
    AppComponent,
    MainMenuComponent,
    PokemonCardComponent,
    DetailedPokemonComponent,
    TypeBlockComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    SpeechSynthesisModule.forRoot({
      lang: 'es',
      volume: 1.0,
      pitch: 1.0,
      rate: 1.0,
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
