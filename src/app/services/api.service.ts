import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url = 'https://pokeapi.co/api/v2/';


  constructor(private http: HttpClient) { }

  getPokemonByRange(start: number, finish: number): Observable<any>{
    return this.http.get(`${this.url}pokemon?offset=${start}&limit=${finish}`);
  }

  getPokemon(pokemon: string): Observable<any>{
    return this.http.get(`https://pokeapi.co/api/v2/pokemon/${pokemon}`);
  }

  getPokemonDetailedInfo(pokemon: string): Observable<any>{
    return this.http.get(`https://pokeapi.co/api/v2/pokemon-species/${pokemon}/`);
  }

  getByFullUrl(url): Observable<any>{
    return this.http.get(url);
  }
}

export class Pokemon {
}
