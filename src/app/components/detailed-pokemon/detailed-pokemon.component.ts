import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { SpeechSynthesisService, SpeechSynthesisUtteranceFactoryService } from '@kamiazya/ngx-speech-synthesis';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-detailed-pokemon',
  templateUrl: './detailed-pokemon.component.html',
  styleUrls: ['./detailed-pokemon.component.sass']
})
export class DetailedPokemonComponent implements OnInit, OnDestroy {

  pokemonData: any;
  pokemonDetailedData: any;
  description = '';
  pokemonEvolutionChain = [];

  @Input() selectedPokemon;
  @Output() selectedPokemonChange = new EventEmitter();


  shiny = false;
  types = [];

  constructor(private pokemonApi: ApiService,
              private f: SpeechSynthesisUtteranceFactoryService,
              private svc: SpeechSynthesisService) { }

  ngOnDestroy(): void{
    this.svc.cancel();
  }

  ngOnInit(): void {
    this.loadData();
  }

  loadData(): void{
    this.types = [];
    this.description = '';
    this.pokemonApi.getPokemon(this.selectedPokemon.name).subscribe(response => {
      this.pokemonData = response;

      response.types.forEach(type => {
        this.types.push(type);
      });
    });

    this.pokemonApi.getPokemonDetailedInfo(this.selectedPokemon.name).subscribe(response => {
      if (response?.evolution_chain?.url && this.pokemonEvolutionChain.length === 0){
        this.pokemonApi.getByFullUrl(response.evolution_chain?.url).subscribe(evoResponse => {
          this.getPokemonEvoutionChain(evoResponse?.chain);
        });
      }

      this.pokemonDetailedData = response;
      this.searchSwordDescription(response);
    });
  }

  click(): void{
    this.selectedPokemonChange.emit(undefined);
  }

  imageClick(): void{
    this.shiny = !this.shiny;
  }

  getPokemonEvoutionChain(pokemonChain): void{
    if (pokemonChain?.species){
      this.pokemonEvolutionChain.push(pokemonChain.species);
    }

    if (pokemonChain?.evolves_to){
      pokemonChain.evolves_to.forEach(pokemon => {
        this.getPokemonEvoutionChain(pokemon);
      });
    }
  }

  emitNewPokemon(): void{
    if (this.svc.speaking){
      this.svc.cancel();
    }
    this.selectedPokemonChange.emit(this.selectedPokemon);
    this.loadData();
  }

  searchSwordDescription(object): void{
    object?.flavor_text_entries.forEach(element => {
      const versionName = element?.version?.name;
      const language = element?.language?.name;
      if ((versionName === 'alpha-sapphire' || versionName === 'ultra-sun' || versionName === 'sword')
        && language === 'es'){
        this.description = (this.description.length <= 0) ? element?.flavor_text : this.description;
      }
    });
  }

  speechDescription(): void{
    const v = this.f.text(this.description.replace(/(\r\n|\n|\r)/gm, ''));
    if (this.svc.speaking){
      this.svc.cancel();
    }else{
      this.svc.speak(v);
    }

  }
}
