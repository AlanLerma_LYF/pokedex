import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { element } from 'protractor';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.sass']
})
export class PokemonCardComponent implements OnInit {

  @Input() pokemon: any;
  pokemonData: any;
  types: any[] = [];

  @Input() selectedPokemon;
  @Output() selectedPokemonChange = new EventEmitter();

  @Input() alternative = false;

  constructor(private pokemonApi: ApiService) {
  }

  ngOnInit(): void {
    this.pokemonApi.getPokemon(this.pokemon.name).subscribe(response => {
      this.pokemonData = response;
      response.types.forEach(type => {
        this.types.push(type);
      });
    });
  }

  onClick(): void{
    this.selectedPokemonChange.emit(this.pokemon);
  }
}
