import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-type-block',
  templateUrl: './type-block.component.html',
  styleUrls: ['./type-block.component.sass']
})
export class TypeBlockComponent implements OnInit {

  @Input() types = [];

  constructor() { }

  ngOnInit(): void {
  }

}
