import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.sass']
})
export class MainMenuComponent implements OnInit {

  pokemonInput = '';

  constructor(private pokeApi: ApiService) { }

  pokemons: any = [];
  selectedPokemon: any;

  ngOnInit(): void {
    this.pokeApi.getPokemonByRange(0, 20).subscribe(response => {
      this.pokemons = response?.results;
    });
  }

  searchPokemon(e): void{
    if (!e || e.keyCode === 13){
      this.selectedPokemon = undefined;
      this.pokemons = [];
      this.pokeApi.getPokemon(this.pokemonInput.toLocaleLowerCase()).subscribe(response => {
        //(response);
        if (response.name){
          this.pokemons.push(response);
        }else if (response.results){
          this.pokemons = response.results;
        }
      });
    }
  }
}
